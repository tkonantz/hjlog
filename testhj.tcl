#!/usr/bin/env tclsh

set hjlog [ open [ lindex $argv 0 ]]

set dg_to_m [ expr 1852.0 * 60.0 ]

fconfigure $hjlog -translation binary

proc dump { s } {

	  binary scan $s H*@0a* hex ascii

	  # Replace non-printing characters in the data.
	  regsub -all -- {[^[:graph:] ]} $ascii {.} ascii

	  # Split the 16 bytes into two 8-byte chunks
	  regexp -- {(.{100})(.{0,100})} $hex -> hex1 hex2

	  # Convert the hex to pairs of hex digits
	  regsub -all -- {..} $hex1 {& } hex1
	  regsub -all -- {..} $hex2 {& } hex2

	  # Put the hex and Latin-1 data to the channel]
	  puts  "[ format { %-150s%-150s} $hex1 $hex2 ]"
}

while { ! [ eof $hjlog ] } {
	set hjrec [ read $hjlog 99 ]
	# append hjrec \x00\x00\x00\x00
	# binary scan $hjrec a4fffffcx23s hdr lat lon lon2 lat2 heading sats alt
	lappend log $hjrec
}


set logrec [ lindex $log [ lindex $argv 1 ]]

dump $logrec

for { set ofs 25 } { $ofs <= 99 } { incr ofs } {
	set testhj [ open "test_$ofs.hj" w ]
	fconfigure $testhj -translation binary
	for { set i 0 } { $i < 256 } { incr i } {
		puts "$ofs $i"
		set outrec [ string replace $logrec $ofs $ofs [ binary format c $i ]]
		# dump $outrec
		puts -nonewline $testhj $outrec
	}
	close $testhj
}




