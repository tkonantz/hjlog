#!/usr/bin/env wish

set cli_cmds { raw full track latlonalt lla }

set field_list { rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
					gVelX gVelY shake vibr volt  alttgt }

#  Check args 

foreach arg $argv {
	if { [ file isfile $arg ] } {
		set hjlog_name $arg
	} elseif { [ lsearch $cli_cmds $arg ] != -1 } {
		set cli_cmd $arg
	} else {
		puts "Skip $arg"
	}
}

proc to_degrees { heading } {
	if { $heading < 0.0 } {
		return [ expr int(round (( 6.28318530717959 + $heading ) * 57.29578 )) % 360 ]
	} else {
		return [ expr int(round ($heading  * 57.29578 )) % 360 ]
	}
}

proc rl96 { v } {
	puts " v $v "
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "R%03d %02x" [ expr - $x ] $v ]"
	} else {
		return "[ format "L%03d %02x" [ expr $x ] $v ]"
	}
}

proc pp96 { v } {
	puts " v $v "
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "push%03d %02x" [ expr - $x ] $v ]"
	} else {
		return "[ format "pull%03d %02x" [ expr $x ] $v ]"
	}
}

proc centr150 { v pos neg} {
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "%s %3d" $neg [ expr abs($x) ]]"
	} else {
		return "[ format "%s %3d" $pos $x ]"
	}
}

proc setpre { v pos neg } {
	if { $v < 0 } {
		return "[ format "%s%2d" $neg [ expr abs($v) ]]"
	} else {
		return "[ format "%s%2d" $pos $v ]"
	}
}

# Both procs use decimal latitude
proc latdeg_to_meters { latd } {
	set latr [ expr $latd * 0.0174533 ]
	return [ expr 111132.92 + (-559.82 * cos(2 * $latr)) + (1.175 * cos(4 * $latr)) + (-0.0023 * cos(6 * $latr))]
}
proc londeg_to_meters { latd } {
	set latr [ expr $latd * 0.0174533 ]
	return [ expr (111412.84 * cos($latr)) + (-93.5 * cos(3 * $latr)) + (0.118 * cos(5 * $latr))]
}


proc load_hj { filename } { 

	global start_location	

	set llfmt "%10.6f"
	set hjlog [ open $filename ]

	fconfigure $hjlog -translation binary

	set rn 1
	while { ! [ eof $hjlog ] } {
		set hjrec [ read $hjlog 99 ]
		# append hjrec \x00\x00\x00\x00		alt = 48 x30
  
		binary scan $hjrec a4fffffccccccccx2cucux4Sx6ssxxcxcx5sx2sx2sx3cx3acucucuasx3ascas  \
			hdr lat lon lon2 lat2 heading sats yr mnth day hr min sec wps \
			mEle mThrot xVelX alt gVelX shake vibr AIud AIlr volt fmode \
			xVelD_lo mRud mAil mEleX xVelY_lo alttgt xVelD_hi flttim amps xVelY_hi gVelY
		
		# translations
		binary scan "$xVelD_lo$xVelD_hi" S xVelD
		binary scan "$xVelY_lo$xVelY_hi" S xVelY
		set volt [ format %5.2f [ expr $volt / 204.8 ]]

		if { 0 } {
			puts -nonewline "[ format "%2d:%02d %4d" $min $sec  $alt ]  "
			puts "$hdr $lat $lon $lon2 $lat2 $heading\n$sats $yr $mnth $day $hr $min $sec $wps \
			$mThrot $xVelX $alt \
			$gVelX $shake $vibr \n$AIud $AIlr $volt $fmode $xVelD_lo $mRud $mAil $mEle $xVelY_lo $alttgt $xVelD_hi \
			$flttim $amps	$xVelY_hi $gVelY"
		} elseif { 0 } {
			# puts -nonewline "wps $wps alt $alt"
			# puts -nonewline "mThrot $mThrot  xVelX  $xVelX  gVelX $gVelX vibr $vibr shake $shake"
			# puts -nonewline "[ format { AI LR %s AI UD %s } [ setpre $AIlr L R ] [ setpre $AIud U D ]] "
			# puts -nonewline "$volt $fmode "
		
			puts -nonewline "  mEle [ centr150 $mEle push pull ] mRud [ centr150 $mRud L R ] $mRud mAil [ centr150 $mAil L R ] $mAil  $mEle talt $alttgt  $flttim $amps  $gVelY $xVelD $xVelY"
			puts ""
		}
		if { 0 } {
			puts "$hdr [ format $llfmt $lat ] [ format $llfmt $lon ] [ format $llfmt$lon2 ] [ format $llfmt $lat2 ] $heading $sats $yr $mnth $day $hr $min $sec $wps $mThrot $xVelX $alt \
				$gVelX $shake $vibr $AIud $AIlr $volt $fmode $xVelD_lo $mRud $mAil $mEle $xVelY_lo $alttgt $xVelD_hi \
				$flttim $amps	$xVelY_hi "
		}

		# set ltime [ clock scan "$mnth/$day/$yr  $hr:$min:$sec" -gmt 1 ]
	
		lappend log [ list $rn $yr $mnth $day $hr $min $sec \
					[ format $llfmt $lat ] [ format $llfmt $lon ] [ to_degrees $heading ] \
					$alt [ format $llfmt $lon2 ] [ format $llfmt $lat2 ] $sats  \
					$fmode $flttim $mRud $mAil $mEle $mThrot $xVelX $xVelY $xVelD $wps  \
					$gVelX $gVelY $shake $vibr $volt  $alttgt ]
		# always bump rn
		incr rn
	}	

	close $hjlog

	return $log
}

proc analyze_log { log } {

	global minlat maxlat min lon minlon
	global starttime startlat startlon lat_m lon_m
	# kep track of howw many records at one time, to the sec
	global occurs
	
	set starttime ""
	
	set minlat 90.0
	set maxlat 0.0
	set minlon 180.0
	set maxlon 0.0

	set starttime "" 
	set startlat 0.0
	set startlon 0.0

	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
							fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
							gVelX gVelY shake vibr volt alttgt

		if { $flttim == 0 && $starttime == ""} {
			set starttime "$mnth/$day/$yr $hr:$min:$sec"
			set startlat $lat
			set startlon $lon
			continue
		}

		if { abs($lat) < abs($minlat) } {
			set minlat $lat
		}
		if { abs($lon) < abs($minlon) } {
			set minlon $lon
		}
		if { abs($lat) > abs($maxlat) } {
			set maxlat $lat
		}
		if { abs($lon) > abs($maxlon) } {
			set maxlon $lon
		}
	}
	set lat_m [ latdeg_to_meters $startlat ]
	set lon_m [ londeg_to_meters $startlat ]
	puts "start lat $startlat lon $startlon time $starttime $lat_m $lon_m\n"
}
# end analyze

proc dumplog { log } {
	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
							fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
							gVelX gVelY shake vibr volt  alttgt

		puts "$rn $yr $mnth $day $hr $min $sec $lat $lon $lon2 $lat2 $heading $alt $sats $fmode $flttim $mRud $mAil $mEle $mThrot $xVelX $xVelY $xVelD $wps $gVelX $gVelY $shake $vibr $volt $alttgt"
	}

}

proc rawlog { log } {
	global field_list
	puts "[ join $field_list \t ]"
	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
					gVelX gVelY shake vibr volt  alttgt

		puts "$rn\t$yr\t$mnth\t$day\t$hr\t$min\t$sec\t$lat\t$lon\t$heading\t$alt\t$lontgt\t$lattgt\t$sats\t$fmode\t$flttim\t$mRud\t$mAil\t$mEle\t$mThrot\t$xVelX\t$xVelY\t$xVelD\t$wps\t$gVelX\t$gVelY\t$shake\t$vibr\t$volt\t$alttgt"
	}
}


proc trackinfo { log outfmt } {

	global startlat startlon lat_m lon_m
	
	set lastdist 0.0
	set lasttime 0
	set gmtofs [ expr (([ clock scan 1/1/70 ] / 60 ) + 24 ) % 24 ]

	puts "startlat, startlon $startlat $startlon"

	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats fmode flttim 
		set alt [ expr $alt / 10.0 ]
		set dx [ expr ($lat - $startlat) * $lat_m ]
		set dy [ expr ($lon - $startlon) * $lon_m ]
		set dist [ expr sqrt($dx*$dx + $dy*$dy ) ]
		if { $outfmt == "track" } {
			set timestr {--}
			catch { set  timestr [ clock format [ clock scan "$mnth/$day/$yr  $hr:$min:$sec" -gmt 1 ] -format %H:%M:%S ] }
			puts "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %7.1f %7.1f %7.1f  %6.1f  %s" \
							$rn $lat $lon $alt $heading $dx $dy $dist [ expr $dist - $lastdist ] $timestr ]"
		} elseif { $outfmt == "lla" } {
			puts "[ format "%10.6f  %10.6f  %5.1f  %3d" $lat $lon $alt $heading ]"
		} elseif { $outfmt == "xxx" } {
			puts "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %2d %7.1f %7.1f %7.1f %9.5f  %9.5f " $rn $lat $lon $alt $headingi $sats $dx $dy $dist $lat2 $lon2  ]"
		}
		set lastdist $dist
	}
}

proc lla { log } {

	puts {"latitude","longitude","altuitude","heading"}

	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats fmode flttim 
		puts "[ format {"%10.6f","%10.6f","%5.1f","%3d"} $lat $lon $alt $heading ]"
	}
}

proc lla_tab { log } {

	# global startlat startlon lat_m lon_m
	set outtxt ""
	append outtxt "Latitude\tLongitude\tAltitude\tHeading"

	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats fmode flttim 
		append outtxt "\n"
		append outtxt [ format "%10.6f\t%10.6f\t%5.1f\t%3d" $lat $lon $alt $heading ]
	}
	return $outtxt
}


proc trackinfoII { log } {

	set n 0
	foreach logrec $log {
		if { [ lindex $logrec 6 ] > 1 && [ lindex $logrec 1 ] > 0.0 } {
			set startlat [ lindex $logrec 1 ]
			set startlon [ lindex $logrec 2 ]
			break
		}
		incr n
	}
	set log [ lrange $log $n end ]

	set latd2m [ latdeg_to_meters $startlat ]
	set lond2m [ londeg_to_meters $startlat ]

	if { 0 } {
		puts "[ join $log \n ]"
	}

	puts {"name","latitude","longitude"}
	puts [ format "Start,%9.6f,%9.6f" $startlat $startlon ]
	puts [ format "NW,%9.6f,%9.6f" $maxlat $minlon ]
	puts [ format "SW,%9.6f,%9.6f" $minlat $minlon ]
	puts [ format "SE,%9.6f,%9.6f" $minlat $maxlon ]
	puts [ format "NE,%9.6f,%9.6f" $maxlat $maxlon ]

	## puts "[ join $log \n ]"
	puts {"type","latitude","longitude"}

	set lastdist 0.0
	set lasttime 0
	set gmtofs [ clock scan 1/1/70 ]

	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon lon2 lat2 headingi alt sats ltim
		set alt [ expr $alt / 10.0 ]
		set dx [ expr ($lat - $startlat) * $latd2m ]
		set dy [ expr ($lon - $startlon) * $lond2m ]
		set dist [ expr sqrt($dx*$dx + $dy*$dy ) ]
		if { $outfmt == "track" } {
			set timestr {--}
			catch { set  timestr [ clock format $ltim -format %H:%M:%S ] }
			puts "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %2d %7.1f %7.1f %7.1f  %6.1f  %s" \
							$rn $lat $lon $alt $headingi $sats $dx $dy $dist [ expr $dist - $lastdist ] $timestr ]"
		} elseif { $outfmt == "xxx" } {
			puts "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %2d %7.1f %7.1f %7.1f %9.5f  %9.5f " $rn $lat $lon $alt $headingi $sats $dx $dy $dist $lat2 $lon2  ]"
		} elseif { $outfmt == "llah" } {
			puts "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %2d " $rn $lat $lon $alt $headingi $sats  ]"
		} elseif { $outfmt == "raw" } {
			## puts -nonewline "[ format "%4d - %10.6f  %10.6f  %5.1f  %3d  %2d " $lat $lon $alt $headingi $sats  ]"
			puts "$log"
		}	
		set lastdist $dist
	}
}


#### Main ####
if { ! [ info exists cli_cmd ] } { 
	
} elseif { $cli_cmd == {raw} } {
	# display full log rec
	puts "Raw log $hjlog_name"
	set log [ load_hj $hjlog_name ]
	rawlog $log
} elseif { $cli_cmd == {full} } {
	# display full log rec
	puts "Full log $hjlog_name"
	set log [ load_hj $hjlog_name ]
	puts "[ join $log \n ]"
	dumplog $log
} elseif { $cli_cmd == {track}} {
	puts "Track Information $hjlog_name"
	set log [ load_hj $hjlog_name ]
	analyze_log $log
	trackinfo $log track
} elseif { $cli_cmd == {lla}} {
	puts "Track Information $hjlog_name"
	set log [ load_hj $hjlog_name ]
	analyze_log $log
	lla $log
} else {
	puts "No command specified"
}
