#!/usr/bin/env wish

# Copyright 2015, Tom Konantz 

# defaults

set debug 0

set allrecs 0

set cur_format all

array set tabsets { \
	raw 		{ 12m 22m 30m 40m 50m 60m 80m center 108m center 132m center 150m center 174m center 196m center 230m center 246m center 262m center 278m center } \
	all 		{ 10m right 25m right 40m right 55m center 85m center 95m center 105m center 150m center 174m center 196m center 230m center 246m center 262m center 278m center } \
	lla	 	{ 50m center 80m center 110m center } \
	track_info 	{ 30m center 60m center 90m center 110m center 140m right 160m right 180m right 200m right  210m left}
	g			{  30m center 62m center 94m center 126m }
	fc			{ 20m right 40m right 60m center 90m center 115m right }}

set tabsets(all) { 10m right 35m right 45m right 60m center 85m center 110m center 125m center 140m center 155m center 170m center }

set format_list [ array names tabsets ]

# used to get numeric offset  with name lookup - maps field name to offset - lsearch
set log_fields { rn yr mnth day hr min sec lat lon heading alt \
						lontgt lattgt sats \
						fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
						gVelX gVelY shake vibr volt alttgt AIud AIlr DTB }
set tl_fields { tsecs t100 $t100inc rofs flttim minsec altm dx dy dist }

set log_fields_sel { lat lon heading fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD  \
					gVelX gVelY shake vibr volt alttgt AIud AIlr DTB }


set tl_fields_sel { minsec dx dy altm dist }

set sbv_start 0
set sbv_stop 0
set sbv_offset 0
set hjlog {}

proc to_degrees { heading } {
	if { $heading < 0.0 } {
		return [ expr int(round (( 6.28318531 + $heading ) * 57.29578 )) % 360 ]
	} else {
		return [ expr int(round ($heading  * 57.29578 )) % 360 ]
	}
}

proc rl96 { v } {
	puts " v $v "
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "R%03d %02x" [ expr - $x ] $v ]"
	} else {
		return "[ format "L%03d %02x" [ expr $x ] $v ]"
	}
}

proc pp96 { v } {
	puts " v $v "
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "push%03d %02x" [ expr - $x ] $v ]"
	} else {
		return "[ format "pull%03d %02x" [ expr $x ] $v ]"
	}
}

proc centr150 { v pos neg} {
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "%s %3d" $neg [ expr abs($x) ]]"
	} else {
		return "[ format "%s %3d" $pos $x ]"
	}
}

proc setpre { v pos neg } {
	if { $v < 0 } {
		return "[ format "%s%2d" $neg [ expr abs($v) ]]"
	} else {
		return "[ format "%s%2d" $pos $v ]"
	}
}

# Both procs use decimal latitude
proc latdeg_to_meters { latd } {
	set latr [ expr $latd * 0.0174533 ]
	return [ expr 111132.92 + (-559.82 * cos(2 * $latr)) + (1.175 * cos(4 * $latr)) + (-0.0023 * cos(6 * $latr))]
}
proc londeg_to_meters { latd } {
	set latr [ expr $latd * 0.0174533 ]
	return [ expr (111412.84 * cos($latr)) + (-93.5 * cos(3 * $latr)) + (0.118 * cos(5 * $latr))]
}


proc load_hj { filename { log {} } } { 

	global start_location debug allrecs

	set llfmt "%10.6f"
	set hjlog_file [ open $filename ]

	fconfigure $hjlog_file -translation binary

	set rn 1
	while { ! [ eof $hjlog_file ] } {
		set hjrec [ read $hjlog_file 99 ]

  		#                  1 0
  		#                                 2 32
  		#                  1              2               3 40
  		#                  1              2               3             4 58
  		#                  1              2               3             4             5 
		binary scan $hjrec a4fffffcccccccccucucucucucucucuSx4axssacucxcxsssx2sx2ssxcsxax2xasx3ascas  \
 			hdr lat lon lon2 lat2 heading sats yr mnth day hr min sec wps \
 			mRud mAil mEle mThrot rRud rAil rEle rThrot \
 			xVelX DTB_hi alt gVelX DTB_lo ppct shake vibr \
 			gRight gBack AIud AIlr volt gDown fmode mAh \
 			xVelD_hi xVelY_hi alttgt xVelD_lo flttim amps xVelY_lo gVelY
	
		binary scan "$DTB_hi$DTB_lo" S DTB
		binary scan "$xVelD_hi$xVelD_lo" S xVelD
		binary scan "$xVelY_hi$xVelY_lo" S xVelY
		set volt [ format %5.2f [ expr $volt / 204.8 ]]
		set gBack [ format {%6.3f} [ expr 0.002522 * $gBack ]]
		set gDown [ format {%6.3f} [ expr 0.002522 * $gDown ]]
		set gRight [ format {%6.3f} [ expr 0.002522 * $gRight ]]

		if { $debug & 4 } {
			## output files in log rec order
			puts "$hdr [ format $llfmt $lat ] [ format $llfmt $lon ] [ format $llfmt $lon2 ] [ format $llfmt $lat2 ] \
			[ format $llfmt $heading ] $sats  $yr $mnth $day $hr $min $sec"
			puts "$wps $mRud $mAil $mEle $mThrot $rRud $rAil $rEle $rThrot"
			puts "$xVelX $DTB_hi $alt $gVelX $ppct $shake $vibr $AIud $AIlr $volt $gDown $fmode $mAh"
			puts "$xVelD_hi $xVelY_hi $alttgt $xVelD_lo $flttim $amps $xVelY_lo $gVelY"
		}
		# set ltime [ clock scan "$mnth/$day/$yr  $hr:$min:$sec" -gmt 1 ]
	
		lappend log [ list $rn $yr $mnth $day $hr $min $sec \
					[ format $llfmt $lat ] [ format $llfmt $lon ] [ to_degrees $heading ] \
					$alt [ format $llfmt $lon2 ] [ format $llfmt $lat2 ] $sats  \
					$fmode $flttim $mRud $mAil $mEle $mThrot \
					$xVelX $xVelY $xVelD $wps $gVelX $gVelY  \
					$shake $vibr $volt $alttgt $AIud $AIlr  \
					$DTB $gBack $gDown $gRight $ppct $mAh \
					$rRud $rAil $rEle $rThrot ]
		#	 0 -  6 	: $rn $yr $mnth $day $hr $min $sec \
		#	 7 -  9 	: $lat $lon $heading
		#	10 - 13	: $alt $lon2 $lat2 $sats
		#	14 - 19	: $fmode $flttim $mRud $mAil $mEle $mThrot 
		#  20 - 25	: $xVelX $xVelY $xVelD $wps $gVelX $gVelY
		#	26 - 31	: $shake $vibr $volt  $alttgt $AIud $AIlr $DTB 
		#  32 - 35	: $gBack $gDown $gRight

		# always bump rn
		incr rn
	}	

	close $hjlog_file

	return $log
}

proc analyze_log { log } {
	global minlat maxlat min lon minlon
	global starttime startlat startlon lat_m lon_m
	global timeline fltlogs
	
	set starttime ""
	
	set minlat 90.0
	set maxlat 0.0
	set minlon 180.0
	set maxlon 0.0

	set starttime "" 
	set startlat 0.0
	set startlon 0.0

	lassign [ lindex $log 0 ] rn yr mnth day hr min sec lat lon
	set startlat $lat
	set startlon $lon

	set last_logtime 0
	
	foreach logrec $log {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt \
						lontgt lattgt sats \
						fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
						gVelX gVelY shake vibr volt alttgt AIud AIlr DTB

		if { $flttim == 0 } {
			set starttime "$mnth/$day/$yr $hr:$min:$sec"
			set startlat $lat
			set startlon $lon
			continue
		}
		if { $mnth == 0 || $day == 0 || abs($lon) < 0.0 | abs($lon) > 360.0 } {
			# skip invalid record
         puts "SKIP $mnth $day $lon $flttim"
			continue
		}
		set tsec 0 
		catch { set tsec [ clock scan "$mnth/$day/$yr $hr:$min:$sec" ] }
		# set tsec "$hr$min$sec"
		if { $tsec < $last_logtime } {
			puts "tsec < last_logtime $tsec < last_logtime"
			continue
		}
		set last_logtime $tsec

		if { abs($lat) < abs($minlat) } {
			set minlat $lat
		}
		if { abs($lon) < abs($minlon) } {
			set minlon $lon
		}
		if { abs($lat) > abs($maxlat) } {
			set maxlat $lat
		}
		if { abs($lon) > abs($maxlon) } {
			set maxlon $lon
		}
		lappend bysec($tsec) $logrec
	}

	set lat_m [ latdeg_to_meters $startlat ]
	set lon_m [ londeg_to_meters $startlat ]

	puts "start lat $startlat lon $startlon time $starttime $lat_m $lon_m\n"
	puts "min/max lat $minlat $maxlat , lon $minlon $maxlon"

	set maxrps 0
	set rofs 0
	if { [ info exists bysec ] } {
		# generate time sorted list of log rec - 
		foreach timkey [ lsort -integer [ array names bysec ]] {
			set rcnt [ llength $bysec($timkey) ]
			puts "time $timkey rcnt $rcnt rofs $rofs"
			if {  $rcnt > $maxrps } {
				set maxrps  [ llength $bysec($timkey) ]
				# puts "MAXRPS $maxrps"
				# puts "[ join  $bysec($timkey) \n ]"
			}
			set t100inc [ expr 100 / $rcnt ]
			set t100	0
			# puts -nonewline "[ clock format $timkey -format "%D %T" ]"
			set tl_ndx 0
			foreach logrec $bysec($timkey) {
				lappend fltlogs $logrec
				lassign $logrec rn yr mnth day hr min sec lat lon
				set dy [ expr ($lat - $startlat) * $lat_m ]
				set dx [ expr ($lon - $startlon) * $lon_m ]
				set dist [ expr sqrt($dx*$dx + $dy*$dy) ]
				set minsec [ format %2d:%02d $min $sec ]
				set altm [ expr [ lindex $logrec 10 ] / 10.0 ]
				set flttim [ lindex $logrec 15 ]
				set dx [ format %6.1f $dx ]
				set dy [ format %6.1f $dy ]
				set dist [ format %6.1f $dist ]
				
				lappend timeline "$timkey $t100 $t100inc $rofs $flttim $minsec $altm $dx $dy $dist"
				## puts " ; $timkey $t100 $t100inc $rofs $flttim"
				incr t100 $t100inc
				incr rofs
			}
			# puts ""
		}
		puts "maxrps $maxrps"
	} else {
		puts "no bysec"
	}
}

proc read_hj_file { hj_filename { cur_log {}} } {
	global cur_format hjlog
	puts "PreLOAD $hj_filename"
	set hjlog [ load_hj $hj_filename $cur_log ]
	puts "PreAnalyze"
	analyze_log $hjlog
	puts "PostAnalyze"

	set wname [ file root [ file tail $hj_filename ] ]
	regsub {T([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})} $wname {T \1 \2/\3 - \4:\5:\6} wname
	# string extra text in file name
	set wname [ string range $wname 0 22 ]
	wm title . "$wname"
	# puts "Call open_hj show_$cur_format "
	show_w_fmt $cur_format
}

proc open_hj {} {
	global hj_filename hjlog timeline fltlogs
	set types {{{HJ Log Files}       {.hj}}}
	set hj_filename [tk_getOpenFile -filetypes $types ]
	if { [ file isfile $hj_filename ] } {
		set hjlog {}
		set timeline {}
		set fltlogs	{}
		read_hj_file $hj_filename
	}
}

# same as open but does not reset current records
proc append_hj {} {
	global hj_filename hjlog cur_format tabsets
	set types {{{HJ Log Files}       {.hj}}}
	set hj_filename [tk_getOpenFile -filetypes $types ]
	if { [ file isfile $hj_filename ] } {
		read_hj_file $hj_filename $hjlog
	}
}

proc set_format { fmt } {
	global cur_format tabsets
	# sets format, and clears text
	set cur_format $fmt
	.logfrm.hdr    configure -tabs $tabsets($cur_format)
	.logfrm.logtxt configure -tabs $tabsets($cur_format)
	.logfrm.hdr		delete 1.0 end
	.logfrm.logtxt	delete 1.0 end
}

proc save_tab {} {
	
	global hjlog hj_filename

	if { [ llength $hjlog ] <= 1 } {
		# no action
		puts "Empty text"
		return 
	}
		
	set save_name  [ tk_getSaveFile -title "Save Tab File"  -initialfile "[ file root $hj_filename ].tab" ]
	
	if { $save_name == "" } {
		return
	}

	set save_name "[ file root $save_name].tab"

	set loghdr [ .logfrm.hdr get 1.0 1.end ]
	set logtxt [ .logfrm.logtxt get 1.0 end ]	


	set fd [ open $save_name w ] 
	puts $fd  "$loghdr"
	puts $fd  "$logtxt"
	close $fd
} 

proc save_csv {} {

	global hjlog

	if { [ llength $hjlog ] <= 1 } {
		# no action
		puts "Empty text"
		return 
	}

	set logtxt [ .logfrm.logtxt get 1.0 end ]

	if { $logtxt == "" } {
		# no action
		puts "Empty text"
		return 
	}

	set loghdr [ .logfrm.hdr get 1.0 1.end ]

	set save_name     [tk_getSaveFile -title "Save CSV File" ]

	set save_name "[ file root $save_name].csv"

	if { $save_name == "" } {
		return
	}

	set logtxt "$loghdr\n$logtxt"	

	regsub -all "\t" $logtxt {","} logtxt
	regsub -all "\n" $logtxt "\"\n\"" logtxt
	regsub   {(.*)"$} $logtxt {"\1} logtxt
	regsub -all {" +} $logtxt {"} logtxt
	
	set fd [ open $save_name w ] 
	puts $fd  "$logtxt"
	close $fd	

} 

proc show_rec { logrec } {
	set field_list { rn yr mth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot rRud rAil rEle rThrot  DTB xVelX xVelY xVelD wps  \
					gVelX gVelY AIud AIlr shake vibr volt PPct mAh gRight gBack gDown alttgt }

	for { set i 0 } { $i < 35 } { incr i } {
		puts "[ format { %2d %8s  %s } $i [ lindex $field_list $i ] [ lindex $logrec $i ]]"
	}
}

proc show_g { hjlog } {
	set_format g

	.logfrm.hdr insert 1.0 "Time\tgBack\tgDown\ttgRight\n"

	foreach logrec $hjlog {
		set recnum [ lindex $logrec 0 ]
		set min [ lindex $logrec 5 ]
		set sec [ lindex $logrec 6 ]
		lassign [ lrange $logrec 33 35 ] gBack gDown gRight
		.logfrm.logtxt insert end [ format " %2d:%02d\t%6sg\t%6sg\t%6sg\n" $min $sec $gBack $gDown $gRight ]
	}
}


proc show_raw { hjlog } {
	# current use raw format
	set_format raw

	.logfrm.hdr insert end "rn\tyr\tmnth\tday\thr\tmin\tsec\tlat\tlon\theading\talt\tlontgt\tlattgt\tsats\tfmode\tflttim\tmRud\tmAil\tmEle\tmThrot\trRud\trAil\trEle\trThrot\tDTB\txVelX\txVelY\txVelD\twps\tgVelX\tgVelY\tAIud\tAIlr\tshake\tvibr\tvolt\t{ppct}%\tmAh\tgBack\tgDown\tgRight\talttgt\n"

	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
					gVelX gVelY shake vibr volt  alttgt AIud AIlr DTB gBack gDown gRight ppct mAh \
					rRud rAil rEle rThrot

		.logfrm.logtxt insert end "$rn\t$yr\t$mnth\t$day\t$hr\t$min\t$sec\t$lat\t$lon\t$heading\t$alt\t$lontgt\t$lattgt\t$sats\t$fmode\t$flttim\t$mRud\t$mAil\t$mEle\t$mThrot\t$rRud\t$rAil\t$rEle\t$rThrot\t$DTB\t$xVelX\t$xVelY\t$xVelD\t$wps\t$gVelX\t$gVelY\t$AIud\t$AIlr\t$shake\t$vibr\t$volt\t${ppct}%\t$mAh\t$gBack\t$gDown\t$gRight\t$alttgt\n"
	}
}

proc show_all { hjlog } {

	set_format all

	set lstft 0
	set lstsec 0

	.logfrm.hdr insert end "\t Time \t FT \tM\tLat\tLon\tHdg\tAlt\tSats\tmRud\tmAil\tmEle\tmThrot\trRud\trAil\trEle\trThrot\tDTB\txVelX\txVelY\txVelD\tWPs\tgVelX\tgVelY\tAIud\tAIlr\tshake\tvibr\tgBack\tgDown\tgRight\tTgt Lat\tTgt Lon\tTgtAlt\tvolt\tPwr%\tmAh\n"

	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
					gVelX gVelY shake vibr volt  alttgt AIud AIlr DTB gBack gDown gRight ppct mAh \
					rRud rAil rEle rThrot
		set lstsec $sec
		set lstft $flttim

		set timstr [ format "%2d:%02d" $min $sec ]
		set ftime [ format "%2d:%02d" [ expr $flttim / 60 ] [ expr $flttim % 60 ]]
		set fmode [ format %X $fmode ]
		set alt [ expr $alt / 10.0 ]
		set alttgt [ expr $alttgt / 10.0 ]
		set mRud [ expr $mRud - 150 ]
		set mAil [ expr $mAil - 150  ]
		set mEle [ expr $mEle - 150  ]
		set mThrot [ expr 200 - $mThrot ]
		set rRud [ expr $rRud - 150  ]
		set rAil [ expr $rAil - 150  ]
		set rEle [ expr $rEle - 150  ]
		set rThrot [ expr 200 - $rThrot ]
		set xVelX	[ expr $xVelX / 10.0 ]
		set xVelY	[ expr $xVelY / 10.0 ]
		set xVelD	[ expr $xVelD / 10.0 ]
		set gVelX	[ expr $gVelX / 10.0 ]
		set gVelY	[ expr $gVelY / 10.0 ]
		.logfrm.logtxt insert end "\t$timstr\t$ftime\t$fmode\t$lat\t$lon\t$heading\t$alt\t$sats\t$mRud\t$mAil\t$mEle\t$mThrot\t$rRud\t$rAil\t$rEle\t$rThrot\t$DTB\t$xVelX\t$xVelY\t$xVelD\t$wps\t$gVelX\t$gVelY\t$AIud\t$AIlr\t$shake\t$vibr\t$gBack\t$gDown\t$gRight\t$lattgt\t$lontgt\t$alttgt\t$volt\t$ppct%\t$mAh\n"
	}
}

proc show_fc { hjlog } {
	# current use fc format
	set_format fc
	.logfrm.hdr insert 1.0  "Time	FltTim	Lat	Lon	Head	Alt	mRud	mAil	mEle	mThrot	rRud	rAil	rEle	rThrot	gBack	gDown	gRight	AIud	AIlr	shake	vibr\n"
	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats \
					fmode flttim mRud mAil mEle mThrot xVelX xVelY xVelD wps  \
					gVelX gVelY shake vibr volt  alttgt AIud AIlr DTB gBack gDown gRight ppct mAh \
					rRud rAil rEle rThrot
		set timestr [ format {%d:%02d} $min $sec ]
		set ftim [ format {%d:%02d} [ expr $flttim / 60 ] [ expr $flttim % 60 ]]
		set alt [ format {%5.1f}  [ expr $alt / 10.0 ]]
		.logfrm.logtxt insert end  "$timestr\t$ftim\t$lat\t$lon\t$heading\t$alt\t$mRud\t$mAil\t$mEle\t$mThrot\t$rRud\t$rAil\t$rEle\t$rThrot\t$gBack\t$gDown\t$gRight\t$AIud\t$AIlr\t$shake\t$vibr\n"
	}
}

proc show_lla { hjlog } {
	# current use raw format
	set_format lla

	.logfrm.hdr insert end "  Latitude\tLongitude\tAltitude\tHeading"
	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats fmode flttim
		.logfrm.logtxt insert end  [ format "%10.6f\t%10.6f\t%5.1f\t%3d\n" $lat $lon [ expr $alt / 10.0 ] $heading ]
	}
}

proc show_track_info { hjlog } {
	global startlat startlon lat_m lon_m

	set_format track_info

	puts "start lat lon $startlat $startlon"

	.logfrm.hdr insert 1.0  "  RN\t  Latitude\tLongitude\tAltitude\tHeading\tdX\tdY\tDist\tdDist\t  DTB  \tTime\n"

	set lastdist 0.0
	set lasttime 0
	set gmtofs [ expr (([ clock scan 1/1/70 ] / 60 ) + 24 ) % 24 ]

	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt lontgt lattgt sats fmode flttim 
		set DTB [ lindex $logrec 32 ]
		set alt [ expr $alt / 10.0 ]
		set dx [ expr ($lat - $startlat) * $lat_m ]
		set dy [ expr ($lon - $startlon) * $lon_m ]
		set dist [ expr sqrt($dx*$dx + $dy*$dy ) ]
		set timestr {--}
		catch { set  timestr [ clock format [ clock scan "$mnth/$day/$yr  $hr:$min:$sec" -gmt 1 ] -format %H:%M:%S ] }
		.logfrm.logtxt insert end "[ format "%4d\t%10.6f\t%10.6f\t%5.1f\t%3d\t%7.1f\t%7.1f\t%7.1f\t%6.1f\t%5.0f\t  %s" \
						$rn $lat $lon $alt $heading $dx $dy $dist [ expr $dist - $lastdist ] $DTB $timestr ]\n"
		set lastdist $dist
	}
}

proc show_w_fmt { ufmt } {
	global allrecs hjlog timeline fltlogs

	if { $allrecs } {
		puts "show_w_fmt $ufmt $allrecs"
		show_$ufmt $hjlog
	} else {
		puts "show_w_fmt $ufmt flight"
		show_$ufmt $fltlogs
	}
}

proc gen1_time { tim } {
	set t100  [ expr $tim % 100 ]
	set t [ expr $tim / 100 ]
	return "[ clock format $t -format %H:%M:%S -gmt 1 ].[ format %02d $t100 ]"
}

proc gen_sbv_times { ftime t100ofs t100inc } {
	global sbv_offset1 sbv_offset100
	puts "$ftime - $sbv_offset1 t100ofs $t100ofs t100inc $t100inc $sbv_offset100"
	set t1 [ expr ( $ftime - $sbv_offset1 ) * 100 + $t100ofs  + $sbv_offset100 ]
	set t2 [ expr $t1 + $t100inc ] 
	# set t2 [ expr 99 + $t1 ] 
	return "[ gen1_time $t1 ],[ gen1_time $t2 ]"
}

proc gen_sbv_file {} {
	global sbv_start sbv_stop sbv_offset sbv_fields
	global log_fields tl_fields  log_fields_sel tl_fields_sel
	global timeline fltlogs
	global hj_filename

	set flds_txt [ .sbv.fields.txt get 1.0 end ]
	regsub "\n+$" $flds_txt {} flds_txt
	set flds [ split $flds_txt \n ]
	puts "START $sbv_start STOP $sbv_stop OFFSET $sbv_offset "
	if { [ string is integer $sbv_start ] } {
		# use integer flight time
	} else {
		# use h:m:s - Future
	}
	if { $sbv_stop <= $sbv_start } {
		set sbv_stop 99999
	}
	# 0 - timeline, 1 logrec
	set lselect {}
	set lofs {}
	if { 0 } {
		puts "log_fields\n$log_fields"
		puts "tl_fields\n$tl_fields"
	}
	foreach fldref $flds {
		if { $fldref != {} } {
			set ofs [ lsearch $tl_fields $fldref ]
			if { $ofs != -1 } {
				lappend lselect 1
				lappend loffset $ofs
			} elseif { [ set ofs [ lsearch $log_fields $fldref ]] } {
				lappend lselect 0
				lappend loffset $ofs
			}
		}
	}
	
	foreach ll $lselect oo $lofs fldref $flds {
		puts "$ll $oo $fldref"
	}
	
	catch { destroy .sbv }
	

	# env is not populated
	if { [ info exists env(USER) ] } {
		set USER $env(USER) 
	} elseif { [ info exists env(USERNAME) ] } {
		set USER $env(USERNAME)
	} else {
		set USER {}
	}
	
	set	 sbvout "\[INFORMATION\]\n"
	append sbvout "\[TITLE\] $hj_filename Captions\n"
	append sbvout "\[AUTHOR\] $USER\n"
	append sbvout "\[SOURCE\] Generated with hjlog.tcl\n"
	append sbvout "\[FILEPATH\] $hj_filename\n"
	append sbvout "\[COMMENT\] Fields: $flds\n"
	append sbvout "\[END INFORMATION\]\n"

	append sbvout "\[SUBTITLE\]\n"

	foreach tl $timeline {
		lassign $tl timkey t100 t100inc rofs flttim minsec
		if { $flttim < $sbv_start || $flttim > $sbv_stop } {
			continue
		}
		# current restriction - t100 must be 0 , t100inc = 99
		if { $t100 != 0 } {
			## puts "SKIP flttim $flttim t100 $t100 t100inc $t100inc"
			continue
		}
		set t100inc 99
		set logrec [ lindex $fltlogs $rofs ]
		if { 0 } {
			puts "\n***   tl   --- $tl ***\n"
			puts "*** logrec --- $logrec ***\n"
		}
		## puts "[ gen_sbv_times $flttim $t100  $t100inc ]"
		append sbvout "[ gen_sbv_times $flttim $t100  $t100inc ]\n"
		foreach lsel $lselect lofs $loffset {
			# puts -nonewline " $lsel $lofs "
			if { $lsel } {
				# puts -nonewline "$lsel [ lindex $tl $lofs ] "
				append sbvout "[ lindex $tl $lofs ]  "
			} else {
				# puts -nonewline "$lsel [ lindex $logrec $lofs ] "
				append sbvout "[ lindex $logrec $lofs ]  "
			}
		}
		append sbvout "\n\n"
	}

	#puts "sbvout\n\n$sbvout"
	
	set sbv_name "[ file root [ file tail $hj_filename ]].sbv"
	set save_name  [ tk_getSaveFile -title "Save sbv Close Caption File" \
							-initialdir "[ file dirname $hj_filename ]" \
							-initialfile "$sbv_name" ]
	if { $save_name == "" } {
		puts "\nNo Save\n$sbvout"
		return
	}

	set save_name "[ file root $save_name].sbv"
	set fd [ open $save_name w ]
	puts $fd "$sbvout"
	close $fd
}
	
proc gen_sbv {} {
	global sbv_start sbv_stop sbv_offset1 sbv_offset100
	global sbv_fields log_fields tl_fields_sel log_fields_sel
	
	if { ! [ info exists sbv_offset1 ] } {
		set sbv_offset1 0
		set sbv_offset100 00
	}
	
	toplevel .sbv
	label .sbv.startlbl -text "Start"
	label .sbv.stoplbl -text "Stop"
	label .sbv.offsetlbl -text "Start Offset"
	label .sbv.ofs100lbl -text "100th Offset"
	entry .sbv.start	-width 10 -justify center -textvariable sbv_start
	entry .sbv.stop	-width 10 -justify center -textvariable sbv_stop
	entry .sbv.offset	-width 10 -justify center -textvariable sbv_offset1
	entry .sbv.ofs100	-width 5 -justify center -textvariable sbv_offset100
	frame .sbv.fields  -padx 8p  -pady 8p
	text	.sbv.fields.txt -width 10 -height 12 -wrap word  -padx 8p \
				-yscrollcommand { .sbv.fields.yscr set }
	scrollbar .sbv.fields.yscr	-orient vertical  -command { .sbv.fields.txt yview }
	button .sbv.generate -text "Generate" -command gen_sbv_file
	grid .sbv.startlbl -row 0 -column 0 
	grid .sbv.start -row 1 -column 0 
	grid .sbv.stoplbl -row 2 -column 0 
	grid .sbv.stop -row 3 -column 0 
	grid .sbv.offsetlbl -row 4 -column 0 
	grid .sbv.offset -row 5 -column 0 
	grid .sbv.ofs100lbl -row 6 -column 0 
	grid .sbv.ofs100 -row 7 -column 0 
	grid .sbv.generate -row 8 -column 0 
	grid .sbv.fields -row 0 -column 1 -rowspan 11
	grid .sbv.fields.txt .sbv.fields.yscr -sticky news
	# first line is in timeline; rest are in logrec
	set lflds [ concat $tl_fields_sel $log_fields_sel   ]
	.sbv.fields.txt insert 1.0 [ join $lflds \n ]
}

proc gen_kml_file {} {
	global kml_start_alt hjlog hj_filename

	set kmlout {<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
 <Placemark>
  <name>}
  append kmlout [ wm title . ]
  if { [ string is integer $kml_start_alt ] && $kml_start_alt > 0 } {
  		set kml_altitude_mode "absolute"
  	} else {
  		set kml_altitude_mode "clampToGround"
  		set kml_start_alt 0
  	}
  append kmlout "</name>
<LineString>
<extrude>0</extrude>
<altitudeMode>$kml_altitude_mode</altitudeMode>
<coordinates>\n"

	# iterate through list - 
	foreach logrec $hjlog {
		lassign $logrec rn yr mnth day hr min sec lat lon heading alt
		if { abs($lon) > 1.000 && abs($lat) > 1.000 } {
			append kmlout [ format "%9.6f,%9.6f,%3.1f\n" $lon $lat  [ expr $alt / 10.0  + $kml_start_alt ] ]
		}
	}
	append kmlout "</coordinates>
	</LineString>
</Placemark>
</kml>\n"

	catch { destroy .kml }
	
	puts "$hj_filename\n[ file dirname $hj_filename ]\n[ file root [ file tail $hj_filename ]].kml"
	set kml_name "[ file root [ file tail $hj_filename ]].kml"
	set save_name  [ tk_getSaveFile -title "Save KML File" \
							-initialdir "[ file dirname $hj_filename ]" \
							-initialfile "$kml_name" ]
	
	if { $save_name == "" } {
		return
	}

	set save_name "[ file root $save_name].kml"
	set fd [ open $save_name w ]
	puts $fd "$kmlout"
	close $fd
}

proc gen_kml {} {
	global kml_start_alt
	toplevel .kml
	label .kml.startlbl -text "Start Altitude"
	entry .kml.startalt	-width 10 -justify center -textvariable kml_start_alt
	button .kml.generate -text "Generate KML" -command gen_kml_file
	grid .kml.startlbl -row 0 -column 0 
	grid .kml.startalt -row 0 -column 1 
	grid .kml.generate -row 1 -column 1 
}

proc show_help {} {
	toplevel .help
	text .help.txt -width 80 -height 40 -wrap word  -padx 8p  \
	-yscrollcommand { .help.yscr set }
	scrollbar .help.yscr -orient vertical  -command { .help.txt yview }
	
	grid .help.txt .help.yscr -sticky news

	set help_txt {
hjlog is tcl/tk script that converts hj log file into human readable form. It is in very early development and there will be many improvements over time.  The current version can be useful as is, but it is not fully functional in many respects.

CC will generate a .sbv "close caption" file for upload to youtube.  Details TBD

KML will generate a KML file that can be used with Google Earth. Details TBD

Eventually, it will be able to draw the course defined in the log file as well as to graph user selected log values over time.  I hope to provide an option to add flight parameters to a SRT close caption file, so that flight parameters can be displayed (as close captions) on video taken during the flight.  This is experiemental so it may not work.  For example, many log records have the same time stamp and it's not obvious how to handle these.

The main function is the "Open HJ File", which reads an HJ file and displays the log data in one of several formats.  "Save Text - Tab" and "Save Text - CSV" will save the current data as either tab delineated or comma separated values.  Currently these are not well tested.

Format - selects which of several formats used to display the log data..  

"Raw" shows the data in basic form with relatovely few tranformations.  For example, mRud uses 150 as "centered". Altitude is shown in centimenters.  Some transforms are applied. The Heading is logged as radians from pi to -pi, but this is changed to degrees, 0 to 360.

"All" shows all parameters.

"TrackInfo" includes calculated values including distance from base, and delta distance.

"Lat/Lon/Alt" shows just lat,lon, altitude, and heading.  It can be used with GPSVisualizer to show the track.  Currently the column headers need to be added though this will probably be fixed.

Currently many parameters are extracted, but the meaning of some of these is not clear and there may be errors.

yr mnth day hr min sec - Each record includes a GPS time stamp though there may be multiple records with the same time.   Sequential records may be more than one second apart.
 
lat lon heading alt 	- Latitude, Longitude, Heading ( 0 N  90 E 180 S 270 W ), Altiude in meters

Target Latitude, Target Longitude, Target Altitude.  In preprogrammed flight mode, the Target Lat/Lon will be the next waypoint, though I'm not sure how it it is used in normal flight. 

Sats	- # of GPS Satellites
WPs	- # of Waypoints - this will increment when a waypoint is uploaded.  I'm not sure what, if anything, it means in normal flight.

Flight Mode 
	0	Manual - Attitude Mode
	1	Auto Hover - GPS mode
	2	Auto Navigation - Preprogrammed Flight
	5	Auto waypoint circling
	6	Semi-automatic
	9  Follow/Photo Me?
	11 Return Home ('B')	

Flight Time - in seconds

mRud mAil mEle mThrot  - Manual Rudder, Aileron, Elevator, Throttle - Manual = User input

rRud rAil rEle rThrot - Real Rudder, Aileron, Elevator, Throttle - I think these are the 'real' inputs generated from the user inputs, but I'm not real confident about these.
	
xVelX xVelY xVelD - Extended Kalman Filter Velocities
	
gVelX gVelY	- GPS X, y velocities  
	
Shake, Vibration	I'm not sure what the units are of teh exact meaning of the terms.

AIud AIlr  - Attitude Indicator, Up/Down, Left/Right
	
DTB - Distance to Base
	
gBack gDown gRight - G ratings

Power Percent - Starts at or near 100% and goes down during the flight

Battery Voltage - Usually ranges from 12.5 to 10.7 V. 

mAh	- Not sure what this means though the unit is used to meaure battery capacity. It is related to power because it tends to stay at a single value until takeoff, then increments by one for each minute of flight.
}
	.help.txt insert 1.0 $help_txt
	.help.txt configure -state disabled
}

	frame .btns -padx 10 -pady 6 -borderwidth 2
	button .btns.file -text "File" -width 10 -borderwidth 1
	menu .mFile
	.mFile add command -label "Open HJ Log" -command open_hj
	.mFile add command -label "Append HJ Log" -command append_hj
	.mFile add command -label "Save Text - Tab" -command save_tab
	.mFile add command -label "Save Text - CSV" -command save_csv
	.mFile add command -label "Quit" -command exit
	bind .btns.file  <1> {tk_popup .mFile %X %Y}
	
	button .btns.format -text "Format" -width 10 
	menu .mFormat
	.mFormat add command -label "Track Info" -command  { show_w_fmt track_info }
	.mFormat add command -label "Lat/Lon/Alt." -command { show_w_fmt lla }
	.mFormat add command -label "Gs" -command  { puts Gx ;  show_w_fmt g }
	.mFormat add command -label "Flight Controls" -command  { show_w_fmt fc }
	.mFormat add command -label "All" -command  { puts ALLx ; show_w_fmt all }
	.mFormat add command -label "Raw" -command  { puts RAWx ; show_w_fmt raw }
	bind .btns.format  <1> {tk_popup .mFormat %X %Y}

	button .btns.sbv -text "CC" -width 10 -borderwidth 1 -command gen_sbv
	button .btns.kml -text "KML" -width 10 -borderwidth 1 -command gen_kml
	# bind .btns.sbv  <1> {tk_popup .mFormat %X %Y}

	checkbutton .btns.flttim -variable allrecs -text "All Records"

	button .btns.help -text "Help" -width 10 -command show_help


	## xset/xview used to sync text widgets
	proc xset {args}  {
		eval [linsert $args 0 .logfrm.xscr set]
		xview moveto [lindex [ .logfrm.xscr get ] 0]
	}
		 # called by the scroll bar
	proc xview {args}  {
		eval [linsert $args 0 .logfrm.logtxt xview ]
		eval [linsert $args 0 .logfrm.hdr xview ]
	}

	frame .logfrm -padx 10 -pady 6 -borderwidth 2

	text .logfrm.logtxt -width 120 -height 40 -tabs {40m 80m 110m}  -wrap none  -padx 3  \
			-bd 1 -tabstyle tabular -yscrollcommand { .logfrm.yscr set } -xscrollcommand xset

	text .logfrm.hdr  -endline 2  -width 120 -height 1 -wrap none  -padx 3 \
			-tabstyle tabular  -xscrollcommand xset

	scrollbar .logfrm.yscr -orient vertical  -command { .logfrm.logtxt yview }

	scrollbar .logfrm.xscr -orient horizontal -command xview

	pack .btns -side top -ipadx 5 -ipady 5  -anchor nw
	pack .btns.file  -ipadx 3 -padx 7 -side left
	pack .btns.format -ipadx 3 -padx 7  -side left
	pack .btns.sbv -ipadx 3 -padx 7  -side left
	pack .btns.kml -ipadx 3 -padx 7  -side left
	pack .btns.flttim -side left
	pack .btns.help 	-ipadx 3 -padx 7  -side left
	pack .logfrm -side top -expand 1 -fill both

	grid .logfrm.hdr    -sticky news
	grid .logfrm.logtxt .logfrm.yscr -sticky news
	grid .logfrm.xscr   -sticky news

   grid rowconfigure .logfrm 0 -weight 0
   grid rowconfigure .logfrm 1 -weight 1
   grid rowconfigure .logfrm 2 -weight 0
   grid columnconfigure .logfrm 0 -weight 1
   grid columnconfigure .logfrm 1 -weight 0

foreach arg $argv {
	if { [ file isfile $arg ] && [ file extension $arg ] == ".hj" } {
		set hj_filename $arg
	} elseif { [ lsearch $format_list $arg ] != -1 } {
		set cur_format $arg
	} else {
		puts "IGNORE $arg"
	}
}

if { [ info exists hj_filename ] } {
		read_hj_file $hj_filename
}

