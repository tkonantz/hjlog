#!/usr/bin/env tclsh

set filename [ lindex $argv 0 ]

# options
# u = unsigned , "" = signed
set sou u

set show_delta 1
set show_vals 1

array set field_names {
0	hdr
1	hdr
2	hdr
3	hdr
4	lat
5	lat
6	{}
7	{}
8	{}
9	{}
10	{}
11	{}
12	lontgt
13	{}
14	{}
15	{}
16	lattgt
17	{}
18	{}
19	{}
20	head
21	{}
22	{}
23	{}
24	sat_cnt
25	yr
26	mnth
27	day
28	hr
29	min
30	sec
31	waypoints
32	MRudderQ
33	MAileronQ
34	MElevatorQ
35	MThrottleQ
36	RRudderQ
37	RAileronQ
38	RElevatorQ
39	RThrottle
40	xVelX
41	{}
42	UNKNOWN	
43	UNKNOWN
44	UNKNOWN
45	UNKNOWN
46	DTB_Hi
47	UNKNOWN
48	Altitude
49	{}
50	GPS_VelX
51	{}
52	DTB_Lo
53	PPct
54	Shake	
55	UNKNOWN
56	Vibrate
57	UNKNOWN
58	gRight
59	{}
60	gBack
61	{}
62	AI_UD
63 {}
64	UNKNOWN
65	UNKNOWN
66	AI_LR
67	{}
68	UNKNOWN
69	UNKNOWN
70	BattV
71	{}
72	gDown
73	{}
74	UNKNOWN
75	FlyingMode
76	mAh
77	{}
78	UNKNOWN
79	xVelD_hi
80	MRudderV
81	MAileronV
82	MElevatorV
83	xVelY_hi
84	AltTgt
85	{}
86	UNKNOWN
87	UNKNOWN
88	UNKNOWN
89	xVelD_lo
90	Flight_time
91	{}
92	AmpsQ
93	xVelY_lo
94	GPS_VelY
95	{}
96	UNKNOWN
97	UNKNOWN
98	UNKNOWN }


set fd [ open $filename ]

fconfigure $fd -translation binary

proc to_degrees { heading } {
	if { $heading < 0.0 } {
		return [ expr int(round (( 6.28318530717959 + $heading ) * 57.29578 )) % 360 ]
	} else {
		return [ expr int(round ($heading  * 57.29578 )) % 360 ]
	}
}

puts "Open $filename"

set last [ lrepeat 99 0 ]

set rn 1

while { ! [ eof $fd ] } {
   set hjr [ read $fd 99 ]
   if { $hjr == "" } {
      break
   }
   binary scan $hjr  x4fffffccccccc lat lon lontgt lattgt headngr \
                     sats yr mnth day hr min sec

   set heading [ to_degrees $headngr ]

   
   binary scan $hjr  @83a@93a@83c@93c@83H2@93H2 xVelYhi xVelYlo y1 y2 h1 h2
   
 
   # puts -nonewline "[ format { %4d - %2d/%02d/%02d %2d:%02d:%02d  %2d - } \
   #		$rn $mnth $day $yr $hr $min $sec $sats ]"
   # puts "$lat $lon $lontgt $lattgt $headngr"
   # puts -nonewline "[ format { %4d - %2s/%02s/%02s %2s:%02s:%02s  %2s - } $rn $mnth $day $yr $hr $min $sec $sats ]"

   binary scan "$xVelYlo$xVelYhi" s xVelY
   # puts -nonewline "[ format {+ %4s  %4s  %2s  %2s  %6s} $y1 $y2 $h1 $h2 $xVelY ]"

   set startofs 31
   binary scan $hjr x${startofs}H*@${startofs}c${sou}* hexn vals 
   regsub -all {..} $hexn {& } hexn
   # puts "$hexn"
   # puts "$vals"
  
   if { ( $rn - 1 ) % 5 == 0 } {
     puts -nonewline "\n            "
     for { set i -$startofs } { $i > -99 } { incr i -1 } {
         puts -nonewline "[ format { %4d- } $i ]"
     }
     puts ""
   }
   if { $show_delta } {
		puts -nonewline "[ format {   ++++++  } $min $sec ]"
		foreach aval $vals lastval $last {
		  set delta [ expr $aval - $lastval ]
		  if { $delta != 0 } {
		  		if { $delta > 0 } { 
		  			set lchar "+" 
		  		} else {
		  			set lchar { }
		  		}
				puts -nonewline "[ format { %s%4d } $lchar $delta ]"
		  } else {
			  puts -nonewline "       "
		  }
		}
	   puts " ++++"
	}
   if { $show_vals } {
		puts -nonewline "[ format {%3d  %02d:%02d = } $heading $min $sec  ]"
   	set fldnum $startofs
		foreach aval $vals {
			### lappend values($fldnum) $aval
			if { 1 } {
				puts -nonewline "[ format {%4d = } $aval ]"
			}
			lappend distlist($fldnum) $aval
			incr fldnum
		}
		puts " ="
	}

   set last $vals
   incr rn
}

if { 0 } {
	foreach fldnum { [ array names values ] } {
		puts "$fldnum $values($fldnum)"
	}
}	
close $fd

puts "Distributions"

for { set i $startofs } { $i < 99 } { incr i } {
	if { [ info exists distlist($i) ] } {
		catch { unset vdist }
		foreach v $distlist($i) {
			if { [ info exists vdist($v) ] } {
				incr vdist($v)
			} else {
				set vdist($v) 1
			}
		}
		set kys [ lsort -integer [ array names vdist ]]
		set fldname $field_names($i)
		if { $fldname == {} } {
			set fldname "$last_fldname +++"
		} else {
			set last_fldname $fldname
		}
		puts -nonewline "[ format {%3d - %3d -- %s } $i [ llength $kys ] $fldname ]\n "
		set m 0 
		foreach ky $kys {
			puts -nonewline "[ format {%3d:%4d  } $ky $vdist($ky) ]"
			if { ( $m + 1 ) % 10 == 0 } {
				puts -nonewline "\n "
			}
			incr m
		}
		puts "\n"
    }
} 

if { 0 } {
for { set i $startofs } { $i < 99 } { incr i } {
    if { [ info exists distlist($i) ] } {
       set slist [ lsort -unique -integer $distlist($i) ]
       puts -nonewline "[ format " %3d %3d - " $i [ llength $slist ] -  ]"
       puts "$slist"
    }
} 
}
