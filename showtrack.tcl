#!/usr/bin/env tclsh

set hjlog [ open [ lindex $argv 0 ]]

fconfigure $hjlog -translation binary

set rn 1
while { ! [ eof $hjlog ] } {
	set hjrec [ read $hjlog 99 ]
	append hjrec \x00\x00\x00\x00
	binary scan $hjrec a4fffffx24s hdr lat lon lon2 lat2 heading alt
	set alt [ expr $alt / 10.0 ]
	set headingi [ expr int(round ( 180.0 - ( $heading * 180 / 3.1415927 ))) ]
	if { [ expr abs($lon) > 1.1 ] } {
		puts "[ format "%10.6f\t%10.6f" $lat $lon ]"
	}
	incr rn
}

close $hjlog




