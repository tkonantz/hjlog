#!/usr/bin/env tclsh

set hjlog [ open [ lindex $argv 0 ]]

fconfigure $hjlog -translation binary

proc m96 { v } {
	set x [ expr $v - 150 ]
	if { $x < 0 } {
		return "[ format "R%03d %02x" [ expr - $x ] $v ]"
	} else {
		return "[ format "L%03d %02x" [ expr $x ] $v ]"
	}
}

set rn 1
while { ! [ eof $hjlog ] } {
	set hjrec [ read $hjlog 99 ]
	append hjrec \x00\x00\x00\x00
	binary scan $hjrec a4ffffa4b hdr lat lon lon2 lat2 d1 sats
	# puts "Rec# $rn - $hdr [ format "%9.5f  %9.5f  %9.5f  %9.5e  " $lat $lon $lat2 $lon2 ] $sats "
	binary scan $hjrec x25cccccc yr mnth day hr min sec
	incr hr -6
	puts -nonewline "[ format {%2d/%02d %2d:%02d:%02d  %4d - } $mnth $day $hr $min $sec $rn ]"

	set ofs [ lindex $argv 1 ]
	binary scan $hjrec x${ofs}c b1
	binary scan $hjrec x${ofs}cu b2
	binary scan $hjrec x${ofs}s s1
	binary scan $hjrec x${ofs}su s2
	binary scan $hjrec x${ofs}i i1
	binary scan $hjrec x${ofs}iu i2
	binary scan $hjrec x${ofs}f f1
	puts -nonewline "[ format { %7s %4d %4d %6d %6d %12d %12d } [ m96 $b2 ] $b1 $b2 $s1 $s2 $i1 $i2 ]"
	catch { puts -nonewline "[ format { %-10.7g } $f1 ]" }
	puts " - $rn - ${ofs}"

	incr rn
}

close $hjlog




