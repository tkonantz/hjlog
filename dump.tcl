#!/usr/bin/tclsh

proc hexdump { filename { channel stdout } } {
     # This is derived from the Tcler's WIKI, page 1599,
     # original author unknown, possibly Kevin Kenny.

     if { [ catch {
        # Open the file, and set up to process it in binary mode.
        set fid [open $filename r]
        fconfigure $fid -translation binary -encoding binary
        set recnum 0
        while { ! [ eof $fid ] } {
        
        		if { [ expr $recnum % 5 ] == 0 } {
          		puts -nonewline "[ format "\n%3d-%3d  " [ expr $recnum + 1 ] [ expr $recnum + 5 ] ]"
      			for { set i 0 } { $i < 100 } { incr i 1 } {
        				puts -nonewline "[ format "%02x " $i ]"
        			}
        			puts {}
        		}
        		incr recnum

           # Record the seek address. Read 16 bytes from the file.
           set addr [ tell $fid ]
           ## set s    [read $fid 16 ]
           set s    [read $fid 99]
				append s \x00

           # Convert the data to hex and to characters.
           binary scan $s H*@0a* hex ascii

           # Replace non-printing characters in the data.
           regsub -all -- {[^[:graph:] ]} $ascii {.} ascii

           # Split the 16 bytes into two 8-byte chunks
           # regexp -- {(.{16})(.{0,16})} $hex -> hex1 hex2
           regexp -- {(.{100})(.{0,100})} $hex -> hex1 hex2

           # Convert the hex to pairs of hex digits
           regsub -all -- {..} $hex1 {& } hex1
           regsub -all -- {..} $hex2 {& } hex2

           # Put the hex and Latin-1 data to the channel
           #puts $channel [ format {%08x %-24s %-24s %-16s} \
           #   $addr $hex1 $hex2 $ascii ]
           #puts $channel [ format {%08x %-150s %-150s %-100s} \
           #   $addr $hex1 $hex2 $ascii ]
           puts $channel [ format {%08x %-150s%-150s} \
              $addr $hex1 $hex2 ]
        }
     } err ] } {
        catch { ::close $fid }
        return -code error $err
     }
     # When we're done, close the file.
     catch { ::close $fid }
     return
 }
 
  #----------------------------------------------------------------------
 #
 # Main program
 #
 #----------------------------------------------------------------------

 if { [info exists argv0] && [string equal $argv0 [info script]] } {
     foreach file $argv {
         puts "$file:"
         hexdump $file
     }
 }
