# hjlog.tcl #

### What is hjlog.tcl? ###

hjlog.tcl is a tcl script that is used to extract data from the .hj log files produced by the Xiro Xplorer quadcopter.

### How do I get it? ###

* Go to https://bitbucket.org/tkonantz/hjlog/src
* Click on hjlog.tcl
* Click on "Raw"
* Save as "hjlog.tcl"

### What does it do? ###

Very early command line versions extracted latitude, logitude, heading and altitude from each log record, with text output.  The output could be used with [GPS Visualizer](http://www.gpsvisualizer.com/)

The first released version uses a simple GUI.  Log files can be opened using a popup menu and viewed in a variety of formats.

Later versions can

* Generate .sbv Close Caption](https://support.google.com/youtube/answer/2734698?hl=en) Files - The .sbv file can be uploaded to youtube ( See [Supported subtitle and closed caption files](https://support.google.com/youtube/answer/2734698?hl=en) and click on "basic Formats).  The .sbv file can be edited after generation.   [Example of CC with Log data ](https://www.youtube.com/watch?v=5kscj138wGc)

* hjlog.tcl can generate KML files which can be used in Google Earth to show either 2D or 3D flight graphs.

This image was generated from a hj log file. "Show Elevation Profile" was used to generate the lower graph. This can be used to show the altitude at different points in the flight as well as the total distance traveled.  The image shows 623m which is the distance traveled in the darker red part of the graph. If nothing was selected or if the whole plot was selected, the distance is 4.76 km.

![T20151219_kml_el.png](https://bitbucket.org/repo/RR4MKy/images/2625648764-T20151219_kml_el.png)

### How do I use it? ###

hjlog is a tcl/tk script so it requires a tcl/tk interpreter.

* Mac OS X, Linux - tcl/tk is usually already installed so the script will run without any extra effort.

* Windows
    * If you can install software, http://www.activestate.com/activetcl is the simplest solution.
    * If you do not have administrator privileges, tclkit can be used. See  http://tclkits.rkeene.org/fossil/wiki/Downloads . Tcl/Tk 8.6 for Windows X86 will work for most people.

### Future Plans ###

Although the primary function of hjlog is to get the logged information into a human readable form, additional capabilities are planned.

* User specified fields may be used in a "custom" format.

* .sbv files can now be generated.   Future improvements may include more than one log record per second, and user-specified formats and labels may be added.  Currently only the numbers are shown. 

* Generation of Track files - These would show the track of a flight, with the option to view data for each track point. The initial version will not have a background. 

* Time-Y Graphs - This will show selected parameters by time. For example, you could graph manual_throttle, real_throttle, and altitude. The X-axis will be flight time.

The [Issue Tracker](https://bitbucket.org/tkonantz/hjlog/issues) can be used to add requests for new features.

### Release Notes ###

TBD - will document changes